/* use strict */

const { stat, mkdir, readFile, writeFile } = require('fs').promises;
const showdown = require('showdown');

const OUT_DIR = './public';

(async () => {
  console.log('Getting content');
  const converter = new showdown.Converter({ headerLevelStart: 2 });
  const log = (await readFile('./log.md')).toString().split('---');
  const content = log.reduce(
    (acc, cur) => `${acc}<article>${converter.makeHtml(cur)}</article>`,
    ''
  );

  console.log('Getting styles');
  const styles = (await readFile('styles.css')).toString();

  console.log('Getting template');
  const template = await readFile('./index.html');

  console.log('Making directory');
  try {
    (await stat(OUT_DIR)).isDirectory();
  } catch (e) {
    await mkdir(OUT_DIR);
  }

  console.log('Composing header');
  const header = `<header>
  <h1>Learning Rust</h1>
</header>`;

  console.log('Writing file');

  await writeFile(
    './public/index.html',
    template
      .toString()
      .replace('<!-- STYLES -->', `<style>${styles}</style>`)
      .replace('<!-- HEADER -->', header)
      .replace('<!-- CONTENT -->', content)
  );
})();
